const csvtojson = require("csvtojson");

function convertor(path) {
  return csvtojson()
    .fromFile(path)
    .then((jsonData) => {
      return jsonData;
    });
}

module.exports = convertor;
